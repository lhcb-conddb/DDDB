<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd">
<DDDB>

<!-- ************************************************************** -->
<!-- * BEAM PIPE                                                  * -->
<!-- * Sections in MagnetRegion                                   * -->
<!-- *   from z = 2270 mm to z = 7620 mm                          * -->
<!-- *                                                            * -->
<!-- * Author: Gloria Corti                                       * -->
<!-- *                                                            * -->
<!-- * Consists of                                                * -->
<!-- *   UX85-3:                                                  * -->
<!-- *     - one flange                                           * -->
<!-- *     - conical section of 10 mrad                           * -->
<!-- *   and corresponding vaccum conical sections                * -->
<!-- ************************************************************** -->

<!-- UX85-3 Pipes of Al and Be -->
 <catalog name = "UX853">
   <logvolref href="#lvUX853InMagnet"/>
   <logvolref href="#lvUX853Flange01"/>
   <logvolref href="#lvUX853Cone02"/>
   <logvolref href="#lvUX853Cone03"/>
   <logvolref href="#lvUX853Cone03Flange"/>
   <logvolref href="#lvUX853Cone04"/>
   <logvolref href="#lvUX853Cone05A"/>
   <logvolref href="#lvUX853Vacuum01"/>
 </catalog>

 <logvol name="lvUX853InMagnet">

     <physvol name   = "pvUX853Flange01"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX853/lvUX853Flange01">
       <posXYZ z = "UX853Flange01Zpos"/>
     </physvol>

     <physvol name   = "pvUX853Cone02"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX853/lvUX853Cone02">
       <posXYZ z = "UX853Cone02Zpos"/>
     </physvol>

     <physvol name   = "pvUX853Cone03"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX853/lvUX853Cone03">
       <posXYZ z = "UX853Cone03Zpos"/>
     </physvol>

     <physvol name   = "pvUX853Cone03Flange"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX853/lvUX853Cone03Flange">
       <posXYZ z = "UX853Cone03FlangeZpos"/>
     </physvol>

     <physvol name   = "pvUX853Cone04"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX853/lvUX853Cone04">
       <posXYZ z = "UX853Cone04Zpos"/>
     </physvol>

     <physvol name   = "pvUX853Cone05A"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX853/lvUX853Cone05A">
       <posXYZ z = "UX853Cone05AZpos"/>
     </physvol>

<!-- Now the vacuum inside -->
      <physvol name   = "pvUX853Vacuum01"
               logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX853/lvUX853Vacuum01">
       <posXYZ z = "0.5*UX853Vacuum01Lenght"/>
     </physvol>

  </logvol>

<!-- UX85-3 Flange  -->
 <logvol name = "lvUX853Flange01" material = "Pipe/PipeAl2219F">
   <cons name          = "UX85-3-Flange01"
         sizeZ         = "UX853Flange01Lenght"
         innerRadiusMZ = "UX853Flange01RadiusZmin"
         innerRadiusPZ = "UX853Flange01RadiusZmax"
         outerRadiusMZ = "UX853Flange01OuterRadius"
         outerRadiusPZ = "UX853Flange01OuterRadius"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Alumimium ~2-2.4*mm thick for flange  -->
 <logvol name = "lvUX853Cone02" material = "Pipe/PipeAl2219F">
   <cons name          = "UX85-3-Cone10mrad-02"
         sizeZ         = "UX853Cone02Lenght"
         innerRadiusMZ = "UX853Cone02RadiusZmin"
         innerRadiusPZ = "UX853Cone02RadiusZmax"
         outerRadiusMZ = "UX853Cone02OuterRadius"
         outerRadiusPZ = "UX853Cone02OuterRadius"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium ~2*mm thick for welding and -->
<!-- support flange  -->
 <logvol name = "lvUX853Cone03" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-03"
         sizeZ         = "UX853Cone03Lenght"
         innerRadiusMZ = "UX853Cone03RadiusZmin"
         innerRadiusPZ = "UX853Cone03RadiusZmax"
         outerRadiusMZ = "UX853Cone03OuterRadius"
         outerRadiusPZ = "UX853Cone03OuterRadius"/>
 </logvol>

<!-- UX85-3 little flange for support, surrounds Cone03 -->
 <logvol name = "lvUX853Cone03Flange" material = "Pipe/PipeBeTV56">
   <tubs name        = "UX85-3-Cone03-Flange"
         sizeZ       = "UX853Cone03FlangeLenght"
         innerRadius = "UX853Cone03FlangeInnerRadius"
         outerRadius = "UX853Cone03FlangeOuterRadius"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium 1.4 mm thick -->
 <logvol name = "lvUX853Cone04" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-04"
         sizeZ         = "UX853Cone04Lenght"
         innerRadiusMZ = "UX853Cone04RadiusZmin"
         innerRadiusPZ = "UX853Cone04RadiusZmax"
         outerRadiusMZ = "UX853Cone04RadiusZmin + UX853Cone04Thick"
         outerRadiusPZ = "UX853Cone04RadiusZmax + UX853Cone04Thick"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium 1.6 mm thick -->
 <logvol name = "lvUX853Cone05A" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-05A"
         sizeZ         = "UX853Cone05ALenght"
         innerRadiusMZ = "UX853Cone05ARadiusZmin"
         innerRadiusPZ = "UX853Cone05ARadiusZmax"
         outerRadiusMZ = "UX853Cone05ARadiusZmin + UX853Cone05AThick"
         outerRadiusPZ = "UX853Cone05ARadiusZmax + UX853Cone05AThick"/>
 </logvol>


<!-- Vacuum in UX853InMagnet -->
 <logvol name = "lvUX853Vacuum01" material = "Vacuum">
   <cons name          = "UX85-3-Vacuum-01"
         sizeZ         = "UX853Vacuum01Lenght"
         outerRadiusMZ = "UX853Flange01RadiusZmin"
         outerRadiusPZ = "UX853Cone05ARadiusZmax"/>
 </logvol>

</DDDB>

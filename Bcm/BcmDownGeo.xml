<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd"[
<!ENTITY BcmParams SYSTEM "git:/Bcm/parameters.xml">
]>
<DDDB>

<!-- ***************************************************************** -->
<!-- * Downstream BCM station                                        * -->
<!-- * Author: Magnus Lieng                                          * -->
<!-- * Description: 8 diamond sensors radially distributed on a G10  * -->
<!-- * plate with an tekapeek mounting device.                       * -->
<!-- ***************************************************************** -->

  &BcmParams;

  <!-- Diamond sensor -->
  <logvol name = "lvBcmDownSensor" material = "Bcm/Diamond" sensdet = "GiGaSensDetTracker/BcmSDet" >
    <box name = "BcmDownSensorBox"
         sizeX = "BcmSensorWidth"
         sizeY = "BcmSensorWidth"
         sizeZ = "BcmSensorThick" />
  </logvol>

  <!-- G10 contact plate -->
  <logvol name = "lvBcmDownContact" material = "Bcm/G10" >
    <tubs name = "BcmDownContactTub"
          sizeZ = "BcmContactThick"
          outerRadius = "BcmDownContactOutRad"
          innerRadius = "BcmDownContactInRad" />
  </logvol>

  <!-- Front part of downstream mount -->
  <logvol name = "lvBcmDownMountFront" material = "Bcm/Tekapeek" >
    <tubs name = "BcmDownMountFrontTub"
          sizeZ = "BcmDownMountFrontLength"
          outerRadius = "BcmDownMountFrontOutRad"
          innerRadius = "BcmDownMountFrontInRad" />
  </logvol>

  <!-- Wire -->
  <logvol name = "lvBcmDownWire" material = "Bcm/Wire" >
    <union name = "BcmDownWireUnion">
      <tubs name = "BcmDownWireStartTub"
            sizeZ = "BcmDownWireStartLength"
            outerRadius = "BcmDownWireOutRad"
            innerRadius = "0.0*mm" />
      <tubs name = "BcmDownWireLongTub"
            sizeZ = "BcmDownWireLongLength"
            outerRadius = "BcmDownWireOutRad"
            innerRadius = "0.0*mm" />
      <posXYZ x = "BcmDownWireLongPosX" z = "BcmDownWireLongPosZ"/>
      <tubs name = "BcmDownWireCrossTub"
            sizeZ = "BcmDownWireCrossLength"
            outerRadius = "BcmDownWireOutRad"
            innerRadius = "0.0*mm" />
      <posXYZ x = "BcmDownWireCrossPosX" z = "BcmDownWireCrossPosZ"/>
      <rotXYZ rotY = "90.0*degree" />
    </union>
  </logvol>

  <!-- Quarter section of back part of downstream mount -->
  <logvol name = "lvBcmDownMountBackSection" material = "Bcm/Tekapeek" >
    <subtraction name = "BcmDownMountBackSectionSub" >
      <tubs name = "BcmDownMountBackSectionTub"
            sizeZ = "BcmDownMountBackSectionLength"
            outerRadius = "BcmDownMountBackSectionOutRad"
            innerRadius = "BcmDownMountBackSecitonInRad"
            startPhiAngle = "44.9*degree"
            deltaPhiAngle = "89.8*degree" />
      <tubs name = "BcmDownMountBackSectionHoleTub"
            sizeZ = "BcmDownMountBackSectionHoleLength"
            outerRadius = "BcmDownMountBackSectionHoleRad"
            innerRadius = "0.0*mm" />
      <posXYZ z = "BcmDownMountBackSectionHolePosZ"
              y = "BcmDownMountBackSectionHolePosY" />
      <rotXYZ rotX = "90.0*degree" />
      <box name = "BcmDownMountBackSectionSlotBox"
           sizeX = "BcmDownMountBackSectionSlotWidth"
           sizeY = "BcmDownMountBackSectionSlotThick"
           sizeZ = "BcmDownMountBackSectionSlotLenght" />
      <posXYZ z = "BcmDownMountBackSectionSlotPosZ"
              y = "BcmDownMountBackSectionSlotPosY" />
    </subtraction>
  </logvol>

  <!-- Tekapeek mount put together -->
  <logvol name = "lvBcmDownMount" >
    <physvol name = "pvBcmDownMountFront"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownMountFront" />
    <physvol name = "pvBcmDownMountBackSection1"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownMountBackSection" >
      <posXYZ z = "BcmDownMountFrontLength/2 + BcmDownMountBackSectionLength/2 + 0.01*mm" />
      <rotXYZ rotZ = "45.0*degree" />
    </physvol>
    <physvol name = "pvBcmDownMountBackSection2"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownMountBackSection" >
      <posXYZ z = "BcmDownMountFrontLength/2 + BcmDownMountBackSectionLength/2 + 0.01*mm" />
      <rotXYZ rotZ = "135.0*degree" />
    </physvol>
    <physvol name = "pvBcmDownMountBackSection3"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownMountBackSection" >
      <posXYZ z = "BcmDownMountFrontLength/2 + BcmDownMountBackSectionLength/2 + 0.01*mm" />
      <rotXYZ rotZ = "225.0*degree" />
    </physvol>
    <physvol name = "pvBcmDownMountBackSection4"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownMountBackSection" >
      <posXYZ z = "BcmDownMountFrontLength/2 + BcmDownMountBackSectionLength/2 + 0.01*mm" />
      <rotXYZ rotZ = "315.0*degree" />
    </physvol>
  </logvol>

  <!-- Complete downstream station -->
  <logvol name = "lvBcmDown" >
    <physvol name = "pvBcmDownMount"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownMount" />
    <physvol name = "pvBcmDownContact"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownContact" >
      <posXYZ z = "BcmDownContactPosZ" />
    </physvol>
    <physvol name = "pvBcmDownWire"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownWire" >
      <transformation>
        <posRPhiZ r = "BcmDownWireRad"
                  phi = "180.0*degree" />
        <rotXYZ rotY = "270.0*degree"/>
        <posXYZ />
        <rotXYZ rotZ = "45.0*degree"/>
      </transformation>
    </physvol>
    <physvol name = "pvBcmDownSensor0"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownSensor">
      <posRPhiZ r = "BcmDownSensorRad"
                phi = "90.0*degree"
                z = "BcmDownSensorPosZ" />
    </physvol>
    <physvol name = "pvBcmDownSensor1"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownSensor">
      <posRPhiZ r = "BcmDownSensorRad"
                phi = "135.0*degree"
                z = "BcmDownSensorPosZ" />
      <rotXYZ rotZ="45.0*degree"/>
    </physvol>
    <physvol name = "pvBcmDownSensor2"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownSensor">
      <posRPhiZ r = "BcmDownSensorRad"
                phi = "180.0*degree"
                z = "BcmDownSensorPosZ" />
    </physvol>
    <physvol name = "pvBcmDownSensor3"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownSensor">
      <posRPhiZ r = "BcmDownSensorRad"
                phi = "225.0*degree"
                z = "BcmDownSensorPosZ" />
      <rotXYZ rotZ="45.0*degree"/>
    </physvol>
    <physvol name = "pvBcmDownSensor4"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownSensor">
      <posRPhiZ r = "BcmDownSensorRad"
                phi = "270.0*degree"
                z = "BcmDownSensorPosZ" />
    </physvol>
    <physvol name = "pvBcmDownSensor5"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownSensor">
      <posRPhiZ r = "BcmDownSensorRad"
                phi = "315.0*degree"
                z = "BcmDownSensorPosZ" />
      <rotXYZ rotZ="45.0*degree"/>
    </physvol>
    <physvol name = "pvBcmDownSensor6"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownSensor">
      <posRPhiZ r = "BcmDownSensorRad"
                phi = "0.0*degree"
                z = "BcmDownSensorPosZ" />
    </physvol>
    <physvol name = "pvBcmDownSensor7"
             logvol = "/dd/Geometry/MagnetRegion/BcmDown/lvBcmDownSensor">
      <posRPhiZ r = "BcmDownSensorRad"
                phi = "45.0*degree"
                z = "BcmDownSensorPosZ" />
      <rotXYZ rotZ="45.0*degree"/>
    </physvol>
  </logvol>
</DDDB>

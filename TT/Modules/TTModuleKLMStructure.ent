<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- ***************************************************************** -->
<!-- * Template for describing the structure of KLM module           * -->
<!-- * above/below beampipe                                          * -->
<!-- * Authors: M. Needham & D. Volyanskyy & J. van Tilburg          * -->
<!-- ***************************************************************** -->

<detelem classID = "9110" name = "&Module;">
  <author> M. Needham </author>
  <version> 1.0 </version>
  <geometryinfo lvname    = "/dd/Geometry/BeforeMagnetRegion/TT/Modules/lvModuleKLM"
                condition = "/dd/Conditions/Alignment/TT/&Layer;&Module;"
                support   = "/dd/Structure/LHCb/BeforeMagnetRegion/TT/&Station;/&Layer;"
                npath     = "pv&Layer;&halfLayer;pv&Layer;&Module;" />
  <param name = "detRegion" type = "int"> 2 </param>
  <param name = "firstReadoutSector" type="int"> &firstReadoutSector; </param>
  <param name = "top_bottom" type = "string"> &top_bottom; </param>
  <param name = "column" type = "int"> &column; </param>
  <param name = "moduleType" type = "string"> "KLM"  </param>
  <param name = "version"  type = "string"> DC09 </param>
  <conditioninfo name = "ProdID" condition = "/dd/Conditions/ReadoutConf/TT/ProductionMap/&Layer;&Module;"/>

  <detelem classID = "9120" name = "Ladder4">
    <author> M. Needham </author>
    <version> 1.0 </version>
    <geometryinfo lvname  = "/dd/Geometry/BeforeMagnetRegion/TT/Modules/lvLadder4"
                  support = "/dd/Structure/LHCb/BeforeMagnetRegion/TT/&Station;/&Layer;/&Module;"
                  npath   = "pvSensorsKLM/pvLadKLM4" />
    <param name = "type" type = "string"> TT4 </param>
    <param name = "subID" type = "int"> 1 </param>
    <param name = "pitch" type="double"> 0.183*mm </param>
    <param name = "numStrips" type="int"> 512 </param>
    <param name = "verticalGuardRing" type="double"> 1.362*mm </param>
    <param name = "bondGap" type="double"> 0.240*mm </param>
    <param name = "capacitance" type="double"> 55*picofarad </param>
    <param name = "nSensors" type = "int"> 4 </param>
    <param name = "version"  type = "string"> DC09 </param>
    <conditioninfo name = "Status" condition = "/dd/Conditions/ChannelInfo/TT/ReadoutSectors/&Layer;&Module;SectorL"/>
    <conditioninfo name = "Noise" condition = "/dd/Conditions/ChannelInfo/TT/NoiseValues/&Layer;&Module;SectorL"/>

    <detelem classID = "9130" name = "Sensor41">
      <author> M. Needham </author>
      <version> 1.0 </version>
      <geometryinfo lvname  = "/dd/Geometry/BeforeMagnetRegion/TT/Modules/lvSensor"
                  condition = "/dd/Conditions/Alignment/TT/&Layer;&Module;Sensor1"
                    support = "/dd/Structure/LHCb/BeforeMagnetRegion/TT/&Station;/&Layer;/&Module;/Ladder4"
                    npath   = "pvSensor41" />
       <param name = "sensorID" type="int"> 1 </param>
    </detelem>

    <detelem classID = "9130" name = "Sensor42">
      <author> M. Needham </author>
      <version> 1.0 </version>
      <geometryinfo lvname  = "/dd/Geometry/BeforeMagnetRegion/TT/Modules/lvSensor"
                  condition = "/dd/Conditions/Alignment/TT/&Layer;&Module;Sensor2"
                    support = "/dd/Structure/LHCb/BeforeMagnetRegion/TT/&Station;/&Layer;/&Module;/Ladder4"
                    npath   = "pvSensor42" />
       <param name = "sensorID" type="int"> 2 </param>
    </detelem>

    <detelem classID = "9130" name = "Sensor43">
      <author> M. Needham </author>
      <version> 1.0 </version>
      <geometryinfo lvname  = "/dd/Geometry/BeforeMagnetRegion/TT/Modules/lvSensor"
                  condition = "/dd/Conditions/Alignment/TT/&Layer;&Module;Sensor3"
                    support = "/dd/Structure/LHCb/BeforeMagnetRegion/TT/&Station;/&Layer;/&Module;/Ladder4"
                    npath   = "pvSensor43" />
      <param name = "sensorID" type="int"> 3 </param>
    </detelem>

    <detelem classID = "9130" name = "Sensor44">
      <author> M. Needham </author>
      <version> 1.0 </version>
      <geometryinfo lvname  = "/dd/Geometry/BeforeMagnetRegion/TT/Modules/lvSensor"
                  condition = "/dd/Conditions/Alignment/TT/&Layer;&Module;Sensor4"
                    support = "/dd/Structure/LHCb/BeforeMagnetRegion/TT/&Station;/&Layer;/&Module;/Ladder4"
                    npath   = "pvSensor44" />
     <param name = "sensorID" type="int"> 4 </param>
    </detelem>
  </detelem>

  <detelem classID = "9120" name = "Ladder2">
    <author> M. Needham </author>
    <version> 1.0 </version>
    <geometryinfo lvname  = "/dd/Geometry/BeforeMagnetRegion/TT/Modules/lvLadder2"
                  support = "/dd/Structure/LHCb/BeforeMagnetRegion/TT/&Station;/&Layer;/&Module;"
                  npath   = "pvSensorsKLM/pvLadKLM2" />
    <param name = "type" type = "string"> TT2 </param>
    <param name = "subID" type = "int"> 2 </param>
    <param name = "pitch" type="double"> 0.183*mm </param>
    <param name = "numStrips" type="int"> 512 </param>
    <param name = "verticalGuardRing" type="double"> 1.362*mm </param>
    <param name = "bondGap" type="double"> 0.240*mm </param>
    <param name = "capacitance" type="double"> 44*picofarad </param>
    <param name = "nSensors" type = "int"> 2 </param>
    <param name = "version"  type = "string"> DC09 </param>
    <conditioninfo name = "Status" condition = "/dd/Conditions/ChannelInfo/TT/ReadoutSectors/&Layer;&Module;SectorM"/>
    <conditioninfo name = "Noise" condition = "/dd/Conditions/ChannelInfo/TT/NoiseValues/&Layer;&Module;SectorM"/>

    <detelem classID = "9130" name = "Sensor21">
      <author> M. Needham </author>
      <version> 1.0 </version>
      <geometryinfo lvname  = "/dd/Geometry/BeforeMagnetRegion/TT/Modules/lvSensor"
                  condition = "/dd/Conditions/Alignment/TT/&Layer;&Module;Sensor5"
                    support = "/dd/Structure/LHCb/BeforeMagnetRegion/TT/&Station;/&Layer;/&Module;/Ladder2"
                    npath   = "pvSensor21" />
       <param name = "sensorID" type="int"> 1 </param>
    </detelem>


    <detelem classID = "9130" name = "Sensor22">
      <author> M. Needham </author>
      <version> 1.0 </version>
      <geometryinfo lvname  = "/dd/Geometry/BeforeMagnetRegion/TT/Modules/lvSensor"
                  condition = "/dd/Conditions/Alignment/TT/&Layer;&Module;Sensor6"
                    support = "/dd/Structure/LHCb/BeforeMagnetRegion/TT/&Station;/&Layer;/&Module;/Ladder2"
                    npath   = "pvSensor22" />
        <param name = "sensorID" type="int"> 2 </param>
    </detelem>
  </detelem>

  <detelem classID = "9120" name = "Ladder1">
    <author> M. Needham </author>
    <version> 1.0 </version>
    <geometryinfo lvname  = "/dd/Geometry/BeforeMagnetRegion/TT/Modules/lvLadder1"
                  support = "/dd/Structure/LHCb/BeforeMagnetRegion/TT/&Station;/&Layer;/&Module;"
                  npath   = "pvSensorsKLM/pvLadKLM1" />
    <param name = "type" type = "string"> TT1 </param>
    <param name = "subID" type = "int"> 3 </param>
    <param name = "pitch" type="double"> 0.183*mm </param>
    <param name = "numStrips" type="int"> 512 </param>
    <param name = "verticalGuardRing" type="double"> 1.362*mm </param>
    <param name = "bondGap" type="double"> 0.240*mm </param>
    <param name = "capacitance" type="double"> 38*picofarad </param>
    <param name = "nSensors" type = "int"> 1 </param>
    <param name = "version"  type = "string"> DC09 </param>
    <conditioninfo name = "Status" condition = "/dd/Conditions/ChannelInfo/TT/ReadoutSectors/&Layer;&Module;SectorK"/>
    <conditioninfo name = "Noise" condition = "/dd/Conditions/ChannelInfo/TT/NoiseValues/&Layer;&Module;SectorK"/>

    <detelem classID = "9130" name = "Sensor11">
      <author> M. Needham </author>
      <version> 1.0 </version>
      <geometryinfo lvname  = "/dd/Geometry/BeforeMagnetRegion/TT/Modules/lvSensor"
                  condition = "/dd/Conditions/Alignment/TT/&Layer;&Module;Sensor7"
                    support = "/dd/Structure/LHCb/BeforeMagnetRegion/TT/&Station;/&Layer;/&Module;/Ladder1"
                    npath   = "pvSensor11" />
       <param name = "sensorID" type="int"> 1 </param>
    </detelem>
  </detelem>
</detelem>

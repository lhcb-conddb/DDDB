<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/structure.dtd">
<DDDB>
<!--
  *****************************************************************************
  * Id: structure.xml,v 1.2 2007/02/27 18:17:40 marcocle Exp $
  *****************************************************************************
-->
<!-- ***************************************************************** -->
<!--                  Define detector elements for Ecal                -->
<!-- ***************************************************************** -->

  <parameter name="OutCell"     value="121.9*mm"     />
  <parameter name="MidCell"     value="OutCell/2.0"  />
  <parameter name="InCell"      value="OutCell/3.0"  />
  <parameter name="OutID"       value="0"  />
  <parameter name="MidID"       value="1"  />
  <parameter name="InID"        value="2"  />
  <parameter name="ASide"       value="1"  />
  <parameter name="CSide"       value="-1"  />

  <detelem name = "Ecal" classID = "8900">

    <author>  Galina Pakhlova Galina.Pakhlova@cern.ch </author>
    <version>             4.0                         </version>

    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Ecal/Installation/Ecal"
                          condition = "/dd/Conditions/Alignment/Ecal/EcalSystem"
                          support = "/dd/Structure/LHCb/DownstreamRegion"
                          npath   = "EcalSubsystem"/>

    <conditioninfo name      = "Hardware"
                          condition = "/dd/Conditions/ReadoutConf/Ecal/Hardware" />

    <conditioninfo name      = "Readout"
                          condition = "/dd/Conditions/ReadoutConf/Ecal/Readout" />

    <conditioninfo name      = "Monitoring"
                          condition = "/dd/Conditions/ReadoutConf/Ecal/Monitoring" />

    <conditioninfo name      = "Gain"
                          condition = "/dd/Conditions/Calibration/Ecal/Gain" />

    <conditioninfo name      = "Reco"
                          condition = "/dd/Conditions/Calibration/Ecal/Reco" />

    <conditioninfo name      = "Calibration"
                          condition = "/dd/Conditions/Calibration/Ecal/Calibration" />

    <conditioninfo name      = "L0Calibration"
                          condition = "/dd/Conditions/Calibration/Ecal/L0Calibration" />

    <conditioninfo name      = "Quality"
                          condition = "/dd/Conditions/Calibration/Ecal/Quality" />

    <conditioninfo name      = "LEDReference"
                          condition = "/dd/Conditions/Calibration/Ecal/LEDReference" />

    <conditioninfo name      = "PileUpOffset"
                          condition = "/dd/Conditions/Calibration/Ecal/PileUpOffset" />


    <detelemref href = "#EcalC"   />
    <detelemref href = "#EcalA"   />

    <userParameter name = "centralHoleX" type="int">         8 </userParameter>
    <userParameter name = "centralHoleY" type="int">         6 </userParameter>
    <userParameter name = "YToXSizeRatio" type = "double" >  1.0 </userParameter>
    <userParameter name = "CodingBit" type = "int"  >        6 </userParameter>
    <userParameter name = "AdcMax"  type="int">              4095 </userParameter>

<!-- definition of the active Z-acceptance in the local frame -->
    <userParameter name="ZOffset" type="double">
      -0.5*EcalMidModLength+0.5*EcalStackLength+EcalMidFrontCoverLength
    </userParameter>
    <userParameter name="ZSize"   type="double"> EcalStackLength        </userParameter>

 </detelem>

<!-- ***************************************************************** -->
  <detelem name = "EcalA"    classID="8901" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Ecal/Installation/EcalLeft"
                  condition = "/dd/Conditions/Alignment/Ecal/EcalASystem"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Ecal"
                  npath   = "EcalA"/>
    <userParameter name="CaloSide" type="int"> ASide </userParameter>
    <detelemref href = "#EcalAOuter"   />
    <detelemref href = "#EcalAMiddle"   />
    <detelemref href = "#EcalAInner"   />
  </detelem>

  <detelem name = "EcalC"    classID="8901" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Ecal/Installation/EcalRight"
                  condition = "/dd/Conditions/Alignment/Ecal/EcalCSystem"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Ecal"
                  npath   = "EcalC"/>

    <userParameter name="CaloSide" type="int"> CSide </userParameter>
    <detelemref href = "#EcalCOuter"   />
    <detelemref href = "#EcalCMiddle"   />
    <detelemref href = "#EcalCInner"   />
  </detelem>

<!-- ***************************************************************** -->
  <detelem name = "EcalAInner"    classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Ecal/Installation/InnSectionLeft"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalA"
                  npath   = "EcalAInner"/>
    <userParameter name="CellSize" type="double"> InCell </userParameter>
    <userParameter name="Area"     type="int"> InID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*EcalInnXSize  </userParameter>
    <userParameter name="YSize" type="double"> EcalInnYSize </userParameter>
  </detelem>

  <detelem name = "EcalAMiddle"    classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Ecal/Installation/MidSectionLeft"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalA"
                  npath   = "EcalAMiddle"/>
    <userParameter name="CellSize" type="double"> MidCell </userParameter>
    <userParameter name="Area"     type="int"> MidID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*EcalMidXSize </userParameter>
    <userParameter name="YSize" type="double"> EcalMidYSize </userParameter>
  </detelem>

  <detelem name = "EcalAOuter"    classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Ecal/Installation/OutSectionLeft"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalA"
                  npath   = "EcalAOuter"/>
    <userParameter name="CellSize" type="double"> OutCell </userParameter>
    <userParameter name="Area"     type="int"> OutID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*EcalOutXSize </userParameter>
    <userParameter name="YSize" type="double"> EcalOutYSize </userParameter>
  </detelem>

  <detelem name = "EcalCInner"    classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Ecal/Installation/InnSectionRight"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalC"
                  npath   = "EcalCInner"/>
    <userParameter name="CellSize" type="double"> InCell </userParameter>
    <userParameter name="Area"     type="int"> InID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*EcalInnXSize </userParameter>
    <userParameter name="YSize" type="double"> EcalInnYSize </userParameter>
  </detelem>

  <detelem name = "EcalCMiddle"    classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Ecal/Installation/MidSectionRight"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalC"
                  npath   = "EcalCMiddle"/>
    <userParameter name="CellSize" type="double"> MidCell </userParameter>
    <userParameter name="Area"     type="int"> MidID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*EcalMidXSize </userParameter>
    <userParameter name="YSize" type="double"> EcalMidYSize </userParameter>
  </detelem>

  <detelem name = "EcalCOuter"    classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Ecal/Installation/OutSectionRight"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalC"
                  npath   = "EcalCOuter"/>
    <userParameter name="CellSize" type="double"> OutCell </userParameter>
    <userParameter name="Area"     type="int"> OutID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*EcalOutXSize </userParameter>
    <userParameter name="YSize" type="double"> EcalOutYSize </userParameter>
  </detelem>

<!-- ***************************************************************** -->
</DDDB>
